import {inject} from '@loopback/core';
import {get, Request, ResponseObject, RestBindings} from '@loopback/rest';
let arrayMonths: string[] = ["abril", "octubre", "julio", "febrero", "diciembre", "junio", "mayo", "enero", "marzo", "septiembre", "agosto", "noviembre"];

/**
 * OpenAPI response for ping()
 */
const PING_RESPONSE: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'PingResponse',
        properties: {
          greeting: {type: 'string'},
          date: {type: 'string'},
          url: {type: 'string'},
          headers: {
            type: 'object',
            properties: {
              'Content-Type': {type: 'string'},
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};

function sortMonth( months: string[] ): string[]{
    var monthsNumber: number[] = months.map(month => {
        if(month == "enero") return 1;
        else if(month == "febrero") return 2;
        else if(month == "marzo") return 3;
        else if(month == "abril") return 4;
        else if(month == "mayo") return 5;
        else if(month == "junio") return 6;
        else if(month == "julio") return 7;
        else if(month == "agosto") return 8;
        else if(month == "septiembre") return 9;
        else if(month == "octubre") return 10;
        else if(month == "noviembre") return 11;
        else if(month == "diciembre") return 12;
        else return -1;
    });
    monthsNumber = monthsNumber.sort((a, b) => a - b);
    months = monthsNumber.map(month => {
        if(month == 1) return "enero";
        else if(month == 2) return "febrero";
        else if(month == 3) return "marzo";
        else if(month == 4) return "abril";
        else if(month == 5) return "mayo";
        else if(month == 6) return "junio";
        else if(month == 7) return "julio";
        else if(month == 8) return "agosto";
        else if(month == 9) return "septiembre";
        else if(month == 10) return "octubre";
        else if(month == 11) return "noviembre";
        else if(month == 12) return "diciembre";
        else return "-1";
    });
    return months;
}
/**
 * A simple controller to bounce back http requests
 */
export class PingController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) {}



  // Map to `GET /ping`
  @get('/ping', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  ping(): object {
    // Reply with a greeting, the current time, the url, and request headers
    return {
      greeting: 'Hello from LoopBack',
      date: new Date(),
      url: this.req.url,
      months : sortMonth(arrayMonths),
      headers: Object.assign({}, this.req.headers),
    };
  }
}
