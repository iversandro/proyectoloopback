import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {Publishing} from '../models';
import {PublishingRepository} from '../repositories';

export class PublishingControllerController {
  constructor(
    @repository(PublishingRepository)
    public publishingRepository : PublishingRepository,
  ) {}

  @post('/publishings', {
    responses: {
      '200': {
        description: 'Publishing model instance',
        content: {'application/json': {schema: getModelSchemaRef(Publishing)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Publishing, {
            title: 'NewPublishing',
            exclude: ['id'],
          }),
        },
      },
    })
    publishing: Omit<Publishing, 'id'>,
  ): Promise<Publishing> {
    return this.publishingRepository.create(publishing);
  }

  @get('/publishings/count', {
    responses: {
      '200': {
        description: 'Publishing model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Publishing) where?: Where<Publishing>,
  ): Promise<Count> {
    return this.publishingRepository.count(where);
  }

  @get('/publishings', {
    responses: {
      '200': {
        description: 'Array of Publishing model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Publishing, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Publishing) filter?: Filter<Publishing>,
  ): Promise<Publishing[]> {
    return this.publishingRepository.find(filter);
  }

  @patch('/publishings', {
    responses: {
      '200': {
        description: 'Publishing PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Publishing, {partial: true}),
        },
      },
    })
    publishing: Publishing,
    @param.where(Publishing) where?: Where<Publishing>,
  ): Promise<Count> {
    return this.publishingRepository.updateAll(publishing, where);
  }

  @get('/publishings/{id}', {
    responses: {
      '200': {
        description: 'Publishing model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Publishing, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Publishing, {exclude: 'where'}) filter?: FilterExcludingWhere<Publishing>
  ): Promise<Publishing> {
    return this.publishingRepository.findById(id, filter);
  }

  @patch('/publishings/{id}', {
    responses: {
      '204': {
        description: 'Publishing PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Publishing, {partial: true}),
        },
      },
    })
    publishing: Publishing,
  ): Promise<void> {
    await this.publishingRepository.updateById(id, publishing);
  }

  @put('/publishings/{id}', {
    responses: {
      '204': {
        description: 'Publishing PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() publishing: Publishing,
  ): Promise<void> {
    await this.publishingRepository.replaceById(id, publishing);
  }

  @del('/publishings/{id}', {
    responses: {
      '204': {
        description: 'Publishing DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.publishingRepository.deleteById(id);
  }
}
