import {Entity, model, property} from '@loopback/repository';

@model()
export class Publishing extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  subject: string;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'string',
    required: true,
  })
  type: string;


  constructor(data?: Partial<Publishing>) {
    super(data);
  }
}

export interface PublishingRelations {
  // describe navigational properties here
}

export type PublishingWithRelations = Publishing & PublishingRelations;
