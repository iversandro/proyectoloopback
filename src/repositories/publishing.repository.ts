import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Publishing, PublishingRelations} from '../models';

export class PublishingRepository extends DefaultCrudRepository<
  Publishing,
  typeof Publishing.prototype.id,
  PublishingRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Publishing, dataSource);

  }
}
